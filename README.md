﻿# ColorPickerView

**本项目是基于开源项目ColorPickerView进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/danielnilsson9/color-picker-view ）追踪到原项目版本**

#### 项目介绍

- 项目名称：颜色选择器
- 所属系列：ohos的第三方组件适配移植
- 功能：颜色选择器
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/danielnilsson9/color-picker-view
- 原项目基线版本：v1.4.0
- 编程语言：Java
- 外部库依赖：无


#### 效果展示：
![avatar](animation.gif)

#### 安装教程

##### 方案一：

1. 编译依赖库 library-release.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
 4.在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.github.danielnilsson9:colorpickerview:1.0.0'
 }
```

#### 使用说明

 1、在java中使用弹窗进行颜色选择
```java
ColorPickerDialog dialog = ColorPickerDialog.newInstance(
    getContext(),
    1,
    "Color Picker",
    null,
    Color.RED.getValue(),
    true,
    MainAbility.this);
dialog.show();
```
 2、在XML中使用
```xml
<com.github.danielnilsson9.colorpickerview.view.ColorPickerView
            app:alphaChannelVisible="true"
            ohos:height="300vp"
            ohos:id="$+id:colorpickerview__color_picker_view"
            ohos:width="300vp"/>
```

#### 版本迭代

- v1.0.0 

- 实现以下功能

	1. 颜色选择器
	2. 在页面中使用颜色选择器

#### 版权和许可信息
- Apache Licence