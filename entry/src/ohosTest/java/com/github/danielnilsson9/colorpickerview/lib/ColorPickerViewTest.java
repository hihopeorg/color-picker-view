package com.github.danielnilsson9.colorpickerview.lib;

import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ColorPickerViewTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testGetColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerView colorPickerView = new ColorPickerView(context);
        colorPickerView.setColor(Color.RED.getValue());
        int color = colorPickerView.getColor();
        assertEquals("test fail", -65536, color);
    }

    public void testGetBorderColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerView colorPickerView = new ColorPickerView(context);
        colorPickerView.setBorderColor(Color.RED.getValue());
        int color = colorPickerView.getBorderColor();
        assertEquals("test fail", -65536, color);
    }

    public void testGetAlphaSliderText() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerView colorPickerView = new ColorPickerView(context);
        colorPickerView.setAlphaSliderText("ok");
        String color = colorPickerView.getAlphaSliderText();
        assertEquals("test fail", "ok", color);
    }
}