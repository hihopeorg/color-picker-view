package com.github.danielnilsson9.colorpickerview.demo;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialog;
import com.github.danielnilsson9.colorpickerview.utils.EventHelper;
import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    @After
    public void tearDown() {
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.github.danielnilsson9.colorpickerview.demo", actualBundleName);
        sleep();
    }

    @Test
    public void testShowDialog() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility mainAbility = EventHelper.startAbility(MainAbility.class);
        assertNotNull("test failed, can not start mainAbility", mainAbility);
        sleep();
        DirectionalLayout showDialog = (DirectionalLayout) mainAbility.findComponentById(ResourceTable.Id_showDialog);
        EventHelper.triggerClickEvent(mainAbility, showDialog);
        sleep();
        Component v = ColorPickerDialog.getView();
        assertNotNull("test failed", v);
        if (v != null) {
            ColorPickerView mColorPicker = (ColorPickerView) v.findComponentById(com.github.danielnilsson9.colorpickerview.ResourceTable.Id_colorpickerview__color_picker_view);
            Button mOkButton = (Button) v.findComponentById(com.github.danielnilsson9.colorpickerview.ResourceTable.Id_button1);
            EventHelper.inputSwipe(mainAbility, mColorPicker, 100, 100, 300, 300, 2000);
            sleep();
            EventHelper.triggerClickEvent(mainAbility, mOkButton);
            sleep();
        }
    }

    @Test
    public void testGoColorPage() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerAbility colorPickerAbility = EventHelper.startAbility(ColorPickerAbility.class);
        assertNotNull("test failed, can not start colorPickerAbility", colorPickerAbility);
        sleep();
    }

    @Test
    public void testColorSelect() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerAbility colorPickerAbility = EventHelper.startAbility(ColorPickerAbility.class);
        assertNotNull("test failed, can not start colorPickerAbility", colorPickerAbility);
        sleep();
        ColorPickerView mColorPickerView = (ColorPickerView) colorPickerAbility.findComponentById(ResourceTable.Id_colorpickerview__color_picker_view);
        int color = mColorPickerView.getColor();
        Button mOkButton = (Button) colorPickerAbility.findComponentById(ResourceTable.Id_okButton);
        EventHelper.inputSwipe(colorPickerAbility, mColorPickerView, 50, 50, 600, 300, 2000);
        sleep();
        EventHelper.inputSwipe(colorPickerAbility, mColorPickerView, 830, 100, 830, 300, 2000);
        sleep();
        EventHelper.inputSwipe(colorPickerAbility, mColorPickerView, 100, 850, 700, 850, 2000);
        sleep();
        assertFalse("test failed", color == mColorPickerView.getColor());
        EventHelper.triggerClickEvent(colorPickerAbility, mOkButton);
        sleep();
    }

    private void sleep() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}