package com.github.danielnilsson9.colorpickerview.lib;

import com.github.danielnilsson9.colorpickerview.view.DrawingUtils;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

public class DrawingUtilsTest extends TestCase {

    public void testVpToPx() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int vp = 100;
        int px = DrawingUtils.vpToPx(context, vp);
        assertEquals("test fail", 300, px);
    }
}