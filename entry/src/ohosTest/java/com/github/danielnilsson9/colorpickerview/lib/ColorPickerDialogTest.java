package com.github.danielnilsson9.colorpickerview.lib;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialog;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ColorPickerDialogTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
    }

    public void testNewInstance() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerDialog dialog = ColorPickerDialog.newInstance(
                context,
                1,
                Color.RED.getValue(),
                null);
        assertNotNull("test fail", dialog);
    }

    public void testTestNewInstance() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ColorPickerDialog dialog = ColorPickerDialog.newInstance(
                context,
                1,
                "Color Picker",
                null,
                Color.RED.getValue(),
                true,
                null);
        assertNotNull("test fail", dialog);
    }
}