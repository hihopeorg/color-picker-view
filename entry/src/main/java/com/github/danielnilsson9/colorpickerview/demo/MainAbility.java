package com.github.danielnilsson9.colorpickerview.demo;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialog;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class MainAbility extends Ability implements ColorPickerDialog.ColorPickerDialogListener {

    private DirectionalLayout showDialog;
    private DirectionalLayout gotoPage;
    private Component vColor;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        showDialog = (DirectionalLayout) findComponentById(ResourceTable.Id_showDialog);
        gotoPage = (DirectionalLayout) findComponentById(ResourceTable.Id_gotoPage);
        vColor = findComponentById(ResourceTable.Id_vColor);

        showDialog.setClickedListener(component -> {
            ColorPickerDialog dialog = ColorPickerDialog.newInstance(
                    getContext(),
                    1,
                    "Color Picker",
                    null,
                    Color.RED.getValue(),
                    true,
                    MainAbility.this);
            dialog.show();
        });

        gotoPage.setClickedListener(component -> {
            Intent intent1 = new Intent();
            intent1.setOperation(new Intent.OperationBuilder()
                    .withAbilityName(ColorPickerAbility.class.getName())
                    .withBundleName(getBundleName())
                    .build());
            startAbility(intent1);
        });

    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        vColor.setBackground(shapeElement);
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }
}
