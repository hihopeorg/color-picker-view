package com.github.danielnilsson9.colorpickerview.demo;

import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

public class ColorPickerAbility extends Ability implements ColorPickerView.OnColorChangedListener, Component.ClickedListener {

    private ColorPickerView mColorPickerView;
    private Component mOldColorPanelView;
    private Component mNewColorPanelView;

    private Button mOkButton;
    private Button mCancelButton;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_color_picker);
        onInit();
    }

    private void onInit() {
        mColorPickerView = (ColorPickerView) findComponentById(ResourceTable.Id_colorpickerview__color_picker_view);
        mOldColorPanelView = findComponentById(ResourceTable.Id_colorpickerview__color_panel_old);
        mNewColorPanelView = findComponentById(ResourceTable.Id_colorpickerview__color_panel_new);

        mOkButton = (Button) findComponentById(ResourceTable.Id_okButton);
        mCancelButton = (Button) findComponentById(ResourceTable.Id_cancelButton);

        mColorPickerView.setOnColorChangedListener(this);

        int color = new DatabaseHelper(getContext()).getPreferences("ohos").getInt("color", Color.RED.getValue());
        mColorPickerView.setColor(color, true);
        setColor(mOldColorPanelView, color);

        mOkButton.setClickedListener(this);
        mCancelButton.setClickedListener(this);
    }

    private void setColor(Component component, int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        component.setBackground(shapeElement);
    }

    @Override
    public void onColorChanged(int newColor) {
        setColor(mNewColorPanelView, newColor);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_okButton:
                Preferences preferences = new DatabaseHelper(getContext()).getPreferences("ohos");
                int color = mColorPickerView.getColor();
                preferences.putInt("color", color);
                terminateAbility();
                break;
            case ResourceTable.Id_cancelButton:
                terminateAbility();
                break;
        }
    }
}
