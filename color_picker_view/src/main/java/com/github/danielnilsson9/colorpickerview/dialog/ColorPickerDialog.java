/*
 * Copyright (C) 2015 Daniel Nilsson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.danielnilsson9.colorpickerview.dialog;

import com.github.danielnilsson9.colorpickerview.ResourceTable;
import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import com.github.danielnilsson9.colorpickerview.view.ColorPickerView.OnColorChangedListener;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.utils.PacMap;

public class ColorPickerDialog extends CommonDialog {

    public interface ColorPickerDialogListener {
        void onColorSelected(int dialogId, int color);

        void onDialogDismissed(int dialogId);
    }

    private int mDialogId = -1;

    private ColorPickerView mColorPicker;
    private Component mOldColorPanel;
    private Component mNewColorPanel;
    private Button mOkButton;

    private ColorPickerDialogListener mListener;


    public static ColorPickerDialog newInstance(Context context, int dialogId, int initialColor, ColorPickerDialogListener listener) {
        return newInstance(context, dialogId, null, null, initialColor, false, listener);
    }

    public static ColorPickerDialog newInstance(Context context, int dialogId, String title, String okButtonText, int initialColor, boolean showAlphaSlider, ColorPickerDialogListener listener) {

        PacMap args = new PacMap();
        args.putIntValue("id", dialogId);
        args.putString("title", title);
        args.putString("ok_button", okButtonText);
        args.putBooleanValue("alpha", showAlphaSlider);
        args.putIntValue("init_color", initialColor);
        ColorPickerDialog frag = new ColorPickerDialog(context, args, listener);
        return frag;
    }

    private static Component v;

    public static Component getView() {
        return v;
    }

    public ColorPickerDialog(Context context, PacMap args, ColorPickerDialogListener listener) {
        super(context);
        mDialogId = args.getIntValue("id");
        int initColor = args.getIntValue("init_color");
        mListener = listener;

        Component v = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_colorpickerview__dialog_color_picker, null, false);

        this.v = v;

        Text titleView = (Text) v.findComponentById(ResourceTable.Id_title);
        mColorPicker = (ColorPickerView) v.findComponentById(ResourceTable.Id_colorpickerview__color_picker_view);
        mOldColorPanel = v.findComponentById(ResourceTable.Id_colorpickerview__color_panel_old);
        mNewColorPanel = v.findComponentById(ResourceTable.Id_colorpickerview__color_panel_new);
        mOkButton = (Button) v.findComponentById(ResourceTable.Id_button1);

        mColorPicker.setOnColorChangedListener(new OnColorChangedListener() {

            @Override
            public void onColorChanged(int newColor) {
                ShapeElement element = new ShapeElement();
                element.setRgbColor(RgbColor.fromArgbInt(newColor));
                mNewColorPanel.setBackground(element);
            }
        });

        mOkButton.setClickedListener(new Component.ClickedListener() {

            @Override
            public void onClick(Component v) {
                if (mListener != null) {
                    mListener.onColorSelected(mDialogId, mColorPicker.getColor());
                }
                hide();
            }

        });


        String title = args.getString("title");

        if (title != null) {
            titleView.setText(title);
        } else {
            titleView.setVisibility(Component.HIDE);
        }


        mColorPicker.setAlphaSliderVisible(args.getBooleanValue("alpha"));

        String ok = args.getString("ok_button");
        if (ok != null) {
            mOkButton.setText(ok);
        }

        mColorPicker.setColor(initColor, true);

        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(initColor));
        mOldColorPanel.setBackground(element);

        int windowHeight = AttrHelper.vp2px(context.getResourceManager().getDeviceCapability().height, context);
        int windowWidth = AttrHelper.vp2px(context.getResourceManager().getDeviceCapability().width, context);
        this.setSize((int) (windowWidth * 0.8), (int) (windowHeight * 0.4));
        this.setContentCustomComponent(v);
    }

    @Override
    public void hide() {
        super.hide();
        if (mListener != null) {
            mListener.onDialogDismissed(mDialogId);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.onDialogDismissed(mDialogId);
        }
    }
}
