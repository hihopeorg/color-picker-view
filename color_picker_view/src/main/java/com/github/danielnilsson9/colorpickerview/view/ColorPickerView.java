/*
 * Copyright (C) 2015 Daniel Nilsson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 *

 * Change Log:
 *
 * 1.1
 * - Fixed buggy measure and layout code. You can now make the view any size you want.
 * - Optimization of the drawing using a bitmap cache, a lot faster!
 * - Support for hardware acceleration for all but the problematic
 *	 part of the view will still be software rendered but much faster!
 *   See comment in drawSatValPanel() for more info.
 * - Support for declaring some variables in xml.
 *
 * 1.2 - 2015-05-08
 * - More bugs in onMeasure() have been fixed, should handle all cases properly now.
 * - View automatically saves its state now.
 * - Automatic border color depending on current theme.
 * - Code cleanup, trackball support removed since they do not exist anymore.
 *
 * 1.3 - 2015-05-10
 * - Fixed hue bar selection did not align with what was shown in the sat/val panel.
 *   Fixed by replacing the linear gardient used before. Now drawing individual lines
 *   of different colors. This was expensive so we now use a bitmap cache for the hue
 *   panel too.
 * - Replaced all RectF used in the layout process with Rect since the
 *   floating point values was causing layout issues (perfect alignment).
 */
package com.github.danielnilsson9.colorpickerview.view;

import com.github.danielnilsson9.colorpickerview.ResourceTable;
import ohos.agp.colors.HsvColor;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.*;
import ohos.agp.utils.*;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

/**
 * Displays a color picker to the user and allow them
 * to select a color. A slider for the alpha channel is
 * also available. Enable it by setting
 * setAlphaSliderVisible(boolean) to true.
 *
 * @author Daniel Nilsson
 */
public class ColorPickerView extends ComponentContainer implements Component.DrawTask, Component.TouchEventListener, Component.EstimateSizeListener, ComponentContainer.ArrangeListener {

    public interface OnColorChangedListener {
        void onColorChanged(int newColor);
    }

    private final static int DEFAULT_BORDER_COLOR = 0xFF6E6E6E;
    private final static int DEFAULT_SLIDER_COLOR = 0xFFBDBDBD;

    private final static int HUE_PANEL_WDITH_DP = 30;
    private final static int ALPHA_PANEL_HEIGH_DP = 20;
    private final static int PANEL_SPACING_DP = 10;
    private final static int CIRCLE_TRACKER_RADIUS_DP = 5;
    private final static int SLIDER_TRACKER_SIZE_DP = 4;
    private final static int SLIDER_TRACKER_OFFSET_DP = 2;

    /**
     * The width in pixels of the border
     * surrounding all color panels.
     */
    private final static int BORDER_WIDTH_PX = 1;

    /**
     * The width in px of the hue panel.
     */
    private int mHuePanelWidthPx;
    /**
     * The height in px of the alpha panel
     */
    private int mAlphaPanelHeightPx;
    /**
     * The distance in px between the different
     * color panels.
     */
    private int mPanelSpacingPx;
    /**
     * The radius in px of the color palette tracker circle.
     */
    private int mCircleTrackerRadiusPx;
    /**
     * The px which the tracker of the hue or alpha panel
     * will extend outside of its bounds.
     */
    private int mSliderTrackerOffsetPx;
    /**
     * Height of slider tracker on hue panel,
     * width of slider on alpha panel.
     */
    private int mSliderTrackerSizePx;


    private Paint mSatValPaint;
    private Paint mSatValTrackerPaint;

    private Paint mAlphaPaint;
    private Paint mAlphaTextPaint;
    private Paint mHueAlphaTrackerPaint;

    private Paint mBorderPaint;

    private Shader mValShader;
    private Shader mSatShader;
    private Shader mAlphaShader;


    /*
     * We cache a bitmap of the sat/val panel which is expensive to draw each time.
     * We can reuse it when the user is sliding the circle picker as long as the hue isn't changed.
     */
    private BitmapCache mSatValBackgroundCache;
    /* We cache the hue background to since its also very expensive now. */
    private BitmapCache mHueBackgroundCache;

    /* Current values */
    private int mAlpha = 0xff;
    private float mHue = 360f;
    private float mSat = 0f;
    private float mVal = 0f;

    private boolean mShowAlphaPanel = false;
    private String mAlphaSliderText = null;
    private int mSliderTrackerColor = DEFAULT_SLIDER_COLOR;
    private int mBorderColor = DEFAULT_BORDER_COLOR;


    /**
     * Minimum required padding. The offset from the
     * edge we must have or else the finger tracker will
     * get clipped when it's drawn outside of the view.
     */
    private int mRequiredPadding;


    /**
     * The Rect in which we are allowed to draw.
     * Trackers can extend outside slightly,
     * due to the required padding we have set.
     */
    private Rect mDrawingRect;

    private Rect mSatValRect;
    private Rect mHueRect;
    private Rect mAlphaRect;

    private Point mStartTouchPoint = null;

    //	private AlphaPatternDrawable 	mAlphaPattern;
    private OnColorChangedListener mListener;


    public ColorPickerView(Context context) {
        super(context);
        init(context, null);
    }

    public ColorPickerView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ColorPickerView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttrSet a) {
        //Load those if set in xml resource file.
        if (a == null) return;
        mShowAlphaPanel = a.getAttr("alphaChannelVisible").isPresent() ? a.getAttr("alphaChannelVisible").get().getBoolValue() : false;
        mAlphaSliderText = a.getAttr("alphaChannelText").isPresent() ? a.getAttr("alphaChannelText").get().getStringValue() : "";
        mSliderTrackerColor = a.getAttr("sliderColor").isPresent() ? a.getAttr("sliderColor").get().getColorValue().getValue() : 0xFFBDBDBD;
        mBorderColor = a.getAttr("borderColor").isPresent() ? a.getAttr("borderColor").get().getColorValue().getValue() : 0xFF6E6E6E;

        mHuePanelWidthPx = DrawingUtils.vpToPx(getContext(), HUE_PANEL_WDITH_DP);
        mAlphaPanelHeightPx = DrawingUtils.vpToPx(getContext(), ALPHA_PANEL_HEIGH_DP);
        mPanelSpacingPx = DrawingUtils.vpToPx(getContext(), PANEL_SPACING_DP);
        mCircleTrackerRadiusPx = DrawingUtils.vpToPx(getContext(), CIRCLE_TRACKER_RADIUS_DP);
        mSliderTrackerSizePx = DrawingUtils.vpToPx(getContext(), SLIDER_TRACKER_SIZE_DP);
        mSliderTrackerOffsetPx = DrawingUtils.vpToPx(getContext(), SLIDER_TRACKER_OFFSET_DP);

        try {
            mRequiredPadding = getResourceManager().getElement(ResourceTable.Float_colorpickerview__required_padding).getInteger();
        } catch (Exception e) {
            e.printStackTrace();
        }

        initPaintTools();

        //Needed for receiving trackball motion events.
        setFocusable(1);
        setTouchFocusable(true);

        addDrawTask(this);
        setTouchEventListener(this);
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    private void initPaintTools() {

        mSatValPaint = new Paint();
        mSatValTrackerPaint = new Paint();
        mHueAlphaTrackerPaint = new Paint();
        mAlphaPaint = new Paint();
        mAlphaTextPaint = new Paint();
        mBorderPaint = new Paint();

        mSatValTrackerPaint.setStyle(Paint.Style.STROKE_STYLE);
        mSatValTrackerPaint.setStrokeWidth(DrawingUtils.vpToPx(getContext(), 2));
        mSatValTrackerPaint.setAntiAlias(true);

        mHueAlphaTrackerPaint.setColor(new Color(mSliderTrackerColor));
        mHueAlphaTrackerPaint.setStyle(Paint.Style.STROKE_STYLE);
        mHueAlphaTrackerPaint.setStrokeWidth(DrawingUtils.vpToPx(getContext(), 2));
        mHueAlphaTrackerPaint.setAntiAlias(true);

        mAlphaTextPaint.setColor(new Color(0xff1c1c1c));
        mAlphaTextPaint.setTextSize(DrawingUtils.vpToPx(getContext(), 14));
        mAlphaTextPaint.setAntiAlias(true);
        mAlphaTextPaint.setTextAlign(TextAlignment.CENTER);
        mAlphaTextPaint.setFakeBoldText(true);

    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mDrawingRect.getWidth() <= 0 || mDrawingRect.getHeight() <= 0) {
            return;
        }

        drawSatValPanel(canvas);
        drawHuePanel(canvas);
        drawAlphaPanel(canvas);
    }

    private void drawSatValPanel(Canvas canvas) {
        final Rect rect = mSatValRect;

        if (BORDER_WIDTH_PX > 0) {
            mBorderPaint.setColor(new Color(mBorderColor));
            canvas.drawRect(mDrawingRect.left, mDrawingRect.top,
                    rect.right + BORDER_WIDTH_PX,
                    rect.bottom + BORDER_WIDTH_PX, mBorderPaint);
        }

        if (mValShader == null) {
            //Black gradient has either not been created or the view has been resized.
            mValShader = new LinearShader(
                    new Point[]{new Point(rect.left, rect.top), new Point(rect.left, rect.bottom)},
                    new float[]{rect.left, rect.top, rect.left, rect.bottom},
                    new Color[]{new Color(0xffffffff), new Color(0xff000000)},
                    Shader.TileMode.CLAMP_TILEMODE
            );
        }


        //If the hue has changed we need to recreate the cache.
        if (mSatValBackgroundCache == null || mSatValBackgroundCache.value != mHue) {

            if (mSatValBackgroundCache == null) {
                mSatValBackgroundCache = new BitmapCache();
            }

            //We create our bitmap in the cache if it doesn't exist.
            if (mSatValBackgroundCache.bitmap == null) {
                PixelMap.InitializationOptions ops = new PixelMap.InitializationOptions();
                ops.size = new Size(rect.getWidth(), rect.getHeight());
                ops.pixelFormat = PixelFormat.ARGB_8888;
                mSatValBackgroundCache.bitmap = PixelMap.create(ops);
            }

            //We create the canvas once so we can draw on our bitmap and the hold on to it.
            if (mSatValBackgroundCache.canvas == null) {
                mSatValBackgroundCache.canvas = new Canvas(new Texture(mSatValBackgroundCache.bitmap));
            }

            int rgb = HsvColor.toColor(255, mHue, 255f, 255f);

            mSatShader = new LinearShader(
                    new Point[]{new Point(rect.left, rect.bottom), new Point(rect.right, rect.bottom)},
                    new float[]{rect.left, rect.bottom, rect.right, rect.bottom},
                    new Color[]{new Color(0xffffffff), new Color(rgb)},
                    Shader.TileMode.CLAMP_TILEMODE
            );

            GroupShader mShader = new GroupShader(mValShader, mSatShader, BlendMode.MULTIPLY);

            mSatValPaint.setShader(mShader, Paint.ShaderType.LINEAR_SHADER);


            // Finally we draw on our canvas, the result will be
            // stored in our bitmap which is already in the cache.
            // Since this is drawn on a canvas not rendered on
            // screen it will automatically not be using the
            // hardware acceleration. And this was the code that
            // wasn't supported by hardware acceleration which mean
            // there is no need to turn it of anymore. The rest of
            // the view will still be hw accelerated.
            mSatValBackgroundCache.canvas.drawRect(0, 0,
                    mSatValBackgroundCache.bitmap.getImageInfo().size.width,
                    mSatValBackgroundCache.bitmap.getImageInfo().size.height,
                    mSatValPaint);

            //We set the hue value in our cache to which hue it was drawn with,
            //then we know that if it hasn't changed we can reuse our cached bitmap.
            mSatValBackgroundCache.value = mHue;

        }

        // We draw our bitmap from the cached, if the hue has changed
        // then it was just recreated otherwise the old one will be used.

        canvas.drawPixelMapHolderRect(new PixelMapHolder(mSatValBackgroundCache.bitmap), new RectFloat(rect), new Paint());

        Point p = satValToPoint(mSat, mVal);

        mSatValTrackerPaint.setColor(new Color(0xff000000));
        canvas.drawCircle(p.getPointX(), p.getPointY(),
                mCircleTrackerRadiusPx - DrawingUtils.vpToPx(getContext(), 1),
                mSatValTrackerPaint);

        mSatValTrackerPaint.setColor(new Color(0xffdddddd));
        canvas.drawCircle(p.getPointX(), p.getPointY(),
                mCircleTrackerRadiusPx, mSatValTrackerPaint);

    }

    private void drawHuePanel(Canvas canvas) {
        final Rect rect = mHueRect;

        if (BORDER_WIDTH_PX > 0) {
            mBorderPaint.setColor(new Color(mBorderColor));

            canvas.drawRect(rect.left - BORDER_WIDTH_PX,
                    rect.top - BORDER_WIDTH_PX,
                    rect.right + BORDER_WIDTH_PX,
                    rect.bottom + BORDER_WIDTH_PX,
                    mBorderPaint);
        }


        if (mHueBackgroundCache == null) {
            mHueBackgroundCache = new BitmapCache();
            PixelMap.InitializationOptions ops = new PixelMap.InitializationOptions();
            ops.pixelFormat = PixelFormat.ARGB_8888;
            ops.size = new Size(rect.getWidth(), rect.getHeight());
            mHueBackgroundCache.bitmap = PixelMap.create(ops);
            mHueBackgroundCache.canvas = new Canvas(new Texture(mHueBackgroundCache.bitmap));


            int[] hueColors = new int[(int) (rect.getHeight() + 0.5f)];

            // Generate array of all colors, will be drawn as individual lines.
            float h = 360f;

            for (int i = 0; i < hueColors.length; i++) {
                hueColors[i] = HsvColor.toColor(255, h, 255f, 255f);
                h -= 360f / hueColors.length;
            }


            // Time to draw the hue color gradient,
            // its drawn as individual lines which
            // will be quite many when the resolution is high
            // and/or the panel is large.
            Paint linePaint = new Paint();
            linePaint.setStrokeWidth(0);
            for (int i = 0; i < hueColors.length; i++) {
                linePaint.setColor(new Color(hueColors[i]));
//                linePaint.setColor(Color.RED);
                mHueBackgroundCache.canvas.drawLine(0, i, mHueBackgroundCache.bitmap.getImageInfo().size.width, i, linePaint);
            }
        }


        canvas.drawPixelMapHolderRect(new PixelMapHolder(mHueBackgroundCache.bitmap), new RectFloat(rect), new Paint());

        Point p = hueToPoint(mHue);

        RectFloat r = new RectFloat();
        r.left = rect.left - mSliderTrackerOffsetPx;
        r.right = rect.right + mSliderTrackerOffsetPx;
        r.top = p.getPointY() - (mSliderTrackerSizePx / 2);
        r.bottom = p.getPointY() + (mSliderTrackerSizePx / 2);

        canvas.drawRoundRect(r, 2, 2, mHueAlphaTrackerPaint);
    }

    private void drawAlphaPanel(Canvas canvas) {
        /*
         * Will be drawn with hw acceleration, very fast.
         * Also the AlphaPatternDrawable is backed by a bitmap
         * generated only once if the size does not change.
         */

        if (!mShowAlphaPanel || mAlphaRect == null
//				|| mAlphaPattern == null
        ) return;

        final Rect rect = mAlphaRect;

        if (BORDER_WIDTH_PX > 0) {
            mBorderPaint.setColor(new Color(mBorderColor));
            canvas.drawRect(rect.left - BORDER_WIDTH_PX,
                    rect.top - BORDER_WIDTH_PX,
                    rect.right + BORDER_WIDTH_PX,
                    rect.bottom + BORDER_WIDTH_PX,
                    mBorderPaint);
        }

//		mAlphaPattern.draw(canvas);
        int color = HsvColor.toColor(255, mHue, mSat * 100, mVal * 100);
        int acolor = HsvColor.toColor(0, mHue, mSat * 100, mVal * 100);

        mAlphaShader = new LinearShader(
                new Point[]{new Point(rect.left, rect.top), new Point(rect.right, rect.top)},
                new float[]{rect.left, rect.top, rect.right, rect.top},
                new Color[]{new Color(color), new Color(acolor)},
                Shader.TileMode.CLAMP_TILEMODE
        );

        mAlphaPaint.setShader(mAlphaShader, Paint.ShaderType.LINEAR_SHADER);

        canvas.drawRect(rect, mAlphaPaint);

        if (mAlphaSliderText != null && !mAlphaSliderText.equals("")) {
            canvas.drawText(mAlphaTextPaint, mAlphaSliderText, rect.getCenterX(),
                    rect.getCenterY() + DrawingUtils.vpToPx(getContext(), 4));
        }

        Point p = alphaToPoint(mAlpha);

        RectFloat r = new RectFloat();
        r.left = p.getPointX() - (mSliderTrackerSizePx / 2);
        r.right = p.getPointX() + (mSliderTrackerSizePx / 2);
        r.top = rect.top - mSliderTrackerOffsetPx;
        r.bottom = rect.bottom + mSliderTrackerOffsetPx;

        canvas.drawRoundRect(r, 2, 2, mHueAlphaTrackerPaint);
    }


    private Point hueToPoint(float hue) {

        final Rect rect = mHueRect;
        final float height = rect.getHeight();

        float y = (height - (hue * height / 360f) + rect.top);
        float x = rect.left;
        Point p = new Point(x, y);

        return p;
    }

    private Point satValToPoint(float sat, float val) {

        final Rect rect = mSatValRect;
        final float height = rect.getHeight();
        final float width = rect.getWidth();


        float x = (sat * width + rect.left);
        float y = ((1f - val) * height + rect.top);
        Point p = new Point(x, y);

        return p;
    }

    private Point alphaToPoint(int alpha) {

        final Rect rect = mAlphaRect;
        final float width = rect.getWidth();

        float x = (width - (alpha * width / 0xff) + rect.left);
        float y = rect.top;
        Point p = new Point(x, y);

        return p;

    }

    private float[] pointToSatVal(float x, float y) {

        final Rect rect = mSatValRect;
        float[] result = new float[2];

        float width = rect.getWidth();
        float height = rect.getHeight();

        if (x < rect.left) {
            x = 0f;
        } else if (x > rect.right) {
            x = width;
        } else {
            x = x - rect.left;
        }

        if (y < rect.top) {
            y = 0f;
        } else if (y > rect.bottom) {
            y = height;
        } else {
            y = y - rect.top;
        }


        result[0] = 1.f / width * x;
        result[1] = 1.f - (1.f / height * y);

        return result;
    }

    private float pointToHue(float y) {

        final Rect rect = mHueRect;

        float height = rect.getHeight();

        if (y < rect.top) {
            y = 0f;
        } else if (y > rect.bottom) {
            y = height;
        } else {
            y = y - rect.top;
        }


        float hue = 360f - (y * 360f / height);

        return hue;
    }

    private int pointToAlpha(int x) {

        final Rect rect = mAlphaRect;
        final int width = (int) rect.getWidth();

        if (x < rect.left) {
            x = 0;
        } else if (x > rect.right) {
            x = width;
        } else {
            x = x - (int) rect.left;
        }

        return 0xff - (x * 0xff / width);

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        boolean update = false;
        MmiPoint point = event.getPointerPosition(event.getIndex());
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mStartTouchPoint = new Point((int) point.getX(), (int) point.getY());
                update = moveTrackersIfNeeded(event);
                break;
            case TouchEvent.POINT_MOVE:
                update = moveTrackersIfNeeded(event);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                mStartTouchPoint = null;
                update = moveTrackersIfNeeded(event);
                break;
        }

        if (update) {
            if (mListener != null) {
                mListener.onColorChanged(HsvColor.toColor(mAlpha, mHue, mSat * 100, mVal * 100));
            }
            invalidate();
            return true;
        }
        return true;
    }

    public boolean contains(Rect rect, int x, int y) {
        return rect.left < rect.right && rect.top < rect.bottom  // check for empty first
                && x >= rect.left && x < rect.right && y >= rect.top && y < rect.bottom;
    }

    private boolean moveTrackersIfNeeded(TouchEvent event) {
        if (mStartTouchPoint == null) {
            return false;
        }

        boolean update = false;
        MmiPoint point = event.getPointerPosition(event.getIndex());
        int startX = mStartTouchPoint.getPointXToInt();
        int startY = mStartTouchPoint.getPointYToInt();

        if (contains(mHueRect, startX, startY)) {
            mHue = pointToHue(point.getY());
            update = true;
        } else if (contains(mSatValRect, startX, startY)) {
            float[] result = pointToSatVal(point.getX(), point.getY());
            mSat = result[0];
            mVal = result[1];
            update = true;
        } else if (mAlphaRect != null && contains(mAlphaRect, startX, startY)) {
            mAlpha = pointToAlpha((int) point.getX());
            update = true;
        }

        return update;
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int finalWidth = 0;
        int finalHeight = 0;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthAllowed = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        int heightAllowed = MeasureSpec.getSize(heightMeasureSpec) - getPaddingBottom() - getPaddingTop();


        //Log.d("color-picker-view", "widthMode: " + modeToString(widthMode) + " heightMode: " + modeToString(heightMode) + " widthAllowed: " + widthAllowed + " heightAllowed: " + heightAllowed);

        if (widthMode == EstimateSpec.PRECISE || heightMode == EstimateSpec.PRECISE) {
            //A exact value has been set in either direction, we need to stay within this size.

            if (widthMode == EstimateSpec.PRECISE && heightMode != EstimateSpec.PRECISE) {
                //The with has been specified exactly, we need to adopt the height to fit.
                int h = (int) (widthAllowed - mPanelSpacingPx - mHuePanelWidthPx);

                if (mShowAlphaPanel) {
                    h += mPanelSpacingPx + mAlphaPanelHeightPx;
                }

                if (h > heightAllowed) {
                    //We can't fit the view in this container, set the size to whatever was allowed.
                    finalHeight = heightAllowed;
                } else {
                    finalHeight = h;
                }

                finalWidth = widthAllowed;

            } else if (heightMode == EstimateSpec.PRECISE && widthMode != EstimateSpec.PRECISE) {
                //The height has been specified exactly, we need to stay within this height and adopt the width.

                int w = (int) (heightAllowed + mPanelSpacingPx + mHuePanelWidthPx);

                if (mShowAlphaPanel) {
                    w -= (mPanelSpacingPx + mAlphaPanelHeightPx);
                }

                if (w > widthAllowed) {
                    //we can't fit within this container, set the size to whatever was allowed.
                    finalWidth = widthAllowed;
                } else {
                    finalWidth = w;
                }

                finalHeight = heightAllowed;

            } else {
                //If we get here the dev has set the width and height to exact sizes. For example match_parent or 300dp.
                //This will mean that the sat/val panel will not be square but it doesn't matter. It will work anyway.
                //In all other senarios our goal is to make that panel square.

                //We set the sizes to exactly what we were told.
                finalWidth = widthAllowed;
                finalHeight = heightAllowed;
            }

        } else {
            //If no exact size has been set we try to make our view as big as possible
            //within the allowed space.

            //Calculate the needed width to layout using max allowed height.
            int widthNeeded = (int) (heightAllowed + mPanelSpacingPx + mHuePanelWidthPx);

            //Calculate the needed height to layout using max allowed width.
            int heightNeeded = (int) (widthAllowed - mPanelSpacingPx - mHuePanelWidthPx);

            if (mShowAlphaPanel) {
                widthNeeded -= (mPanelSpacingPx + mAlphaPanelHeightPx);
                heightNeeded += mPanelSpacingPx + mAlphaPanelHeightPx;
            }

            boolean widthOk = false;
            boolean heightOk = false;

            if (widthNeeded <= widthAllowed) {
                widthOk = true;
            }

            if (heightNeeded <= heightAllowed) {
                heightOk = true;
            }


            //Log.d("color-picker-view", "Size - Allowed w: " + widthAllowed + " h: " + heightAllowed + " Needed w:" + widthNeeded + " h: " + heightNeeded);


            if (widthOk && heightOk) {
                finalWidth = widthAllowed;
                finalHeight = heightNeeded;
            } else if (!heightOk && widthOk) {
                finalHeight = heightAllowed;
                finalWidth = widthNeeded;
            } else if (!widthOk && heightOk) {
                finalHeight = heightNeeded;
                finalWidth = widthAllowed;
            } else {
                finalHeight = heightAllowed;
                finalWidth = widthAllowed;
            }

        }

        //Log.d("color-picker-view", "Final Size: " + finalWidth + "x" + finalHeight);

        setEstimatedSize(finalWidth + getPaddingLeft() + getPaddingRight(),
                finalHeight + getPaddingTop() + getPaddingBottom());
        return false;
    }

    private int getPreferredWidth() {
        //Our preferred width and height is 200dp for the square sat / val rectangle.
        int width = DrawingUtils.vpToPx(getContext(), 200);

        return (int) (width + mHuePanelWidthPx + mPanelSpacingPx);
    }

    private int getPreferredHeight() {
        int height = DrawingUtils.vpToPx(getContext(), 200);

        if (mShowAlphaPanel) {
            height += mPanelSpacingPx + mAlphaPanelHeightPx;
        }
        return height;
    }

    @Override
    public int getPaddingTop() {
        return Math.max(super.getPaddingTop(), mRequiredPadding);
    }

    @Override
    public int getPaddingBottom() {
        return Math.max(super.getPaddingBottom(), mRequiredPadding);
    }

    @Override
    public int getPaddingLeft() {
        return Math.max(super.getPaddingLeft(), mRequiredPadding);
    }

    @Override
    public int getPaddingRight() {
        return Math.max(super.getPaddingRight(), mRequiredPadding);
    }

    @Override
    public boolean onArrange(int w, int h, int oldw, int oldh) {
        mDrawingRect = new Rect();
        mDrawingRect.left = getPaddingLeft();
        mDrawingRect.right = oldw - getPaddingRight();
        mDrawingRect.top = getPaddingTop();
        mDrawingRect.bottom = oldh - getPaddingBottom();

        //The need to be recreated because they depend on the size of the view.
        mValShader = null;
        mSatShader = null;
        mAlphaShader = null;

        // Clear those bitmap caches since the size may have changed.
        mSatValBackgroundCache = null;
        mHueBackgroundCache = null;

        setUpSatValRect();
        setUpHueRect();
        setUpAlphaRect();
        return false;
    }

    private void setUpSatValRect() {
        //Calculate the size for the big color rectangle.
        final Rect dRect = mDrawingRect;

        int left = dRect.left + BORDER_WIDTH_PX;
        int top = dRect.top + BORDER_WIDTH_PX;
        int bottom = dRect.bottom - BORDER_WIDTH_PX;
        int right = dRect.right - BORDER_WIDTH_PX - mPanelSpacingPx - mHuePanelWidthPx;


        if (mShowAlphaPanel) {
            bottom -= (mAlphaPanelHeightPx + mPanelSpacingPx);
        }

        mSatValRect = new Rect(left, top, right, bottom);
    }

    private void setUpHueRect() {
        //Calculate the size for the hue slider on the left.
        final Rect dRect = mDrawingRect;

        int left = dRect.right - mHuePanelWidthPx + BORDER_WIDTH_PX;
        int top = dRect.top + BORDER_WIDTH_PX;
        int bottom = dRect.bottom - BORDER_WIDTH_PX - (mShowAlphaPanel ? (mPanelSpacingPx + mAlphaPanelHeightPx) : 0);
        int right = dRect.right - BORDER_WIDTH_PX;

        mHueRect = new Rect(left, top, right, bottom);
    }

    private void setUpAlphaRect() {

        if (!mShowAlphaPanel) return;

        final Rect dRect = mDrawingRect;

        int left = dRect.left + BORDER_WIDTH_PX;
        int top = dRect.bottom - mAlphaPanelHeightPx + BORDER_WIDTH_PX;
        int bottom = dRect.bottom - BORDER_WIDTH_PX;
        int right = dRect.right - BORDER_WIDTH_PX;

        mAlphaRect = new Rect(left, top, right, bottom);


//		mAlphaPattern = new AlphaPatternDrawable(DrawingUtils.vpToPx(getContext(), 5));
//		mAlphaPattern.setBounds(Math.round(mAlphaRect.left), Math
//				.round(mAlphaRect.top), Math.round(mAlphaRect.right), Math
//				.round(mAlphaRect.bottom));
    }


    /**
     * Set a OnColorChangedListener to get notified when the color
     * selected by the user has changed.
     *
     * @param listener
     */
    public void setOnColorChangedListener(OnColorChangedListener listener) {
        mListener = listener;
    }

    /**
     * Get the current color this view is showing.
     *
     * @return the current color.
     */
    public int getColor() {
        return HsvColor.toColor(mAlpha, mHue, mSat * 100, mVal * 100);
    }

    /**
     * Set the color the view should show.
     *
     * @param color The color that should be selected. #argb
     */
    public void setColor(int color) {
        setColor(color, false);
    }

    /**
     * Set the color this view should show.
     *
     * @param color    The color that should be selected. #argb
     * @param callback If you want to get a callback to
     *                 your OnColorChangedListener.
     */
    public void setColor(int color, boolean callback) {

        int alpha = RgbColor.fromArgbInt(color).getAlpha();
        int red = RgbColor.fromArgbInt(color).getRed();
        int blue = RgbColor.fromArgbInt(color).getBlue();
        int green = RgbColor.fromArgbInt(color).getGreen();

        int color1 = Color.argb(alpha, red, green, blue);
        HsvColor hsvColor = HsvColor.toHSV(color1);

        mAlpha = alpha;

        mHue = hsvColor.getHue();
        mSat = hsvColor.getSaturation();
        mVal = hsvColor.getValue();

        if (callback && mListener != null) {
            mListener.onColorChanged(HsvColor.toColor(mAlpha, mHue, mSat, mVal));
        }

        invalidate();
    }

    /**
     * Set if the user is allowed to adjust the alpha panel. Default is false.
     * If it is set to false no alpha will be set.
     *
     * @param visible
     */
    public void setAlphaSliderVisible(boolean visible) {
        if (mShowAlphaPanel != visible) {
            mShowAlphaPanel = visible;

            /*
             * Force recreation.
             */
            mValShader = null;
            mSatShader = null;
            mAlphaShader = null;
            mHueBackgroundCache = null;
            mSatValBackgroundCache = null;

            postLayout();
        }

    }

    /**
     * Set the color of the tracker slider on the hue and alpha panel.
     *
     * @param color
     */
    public void setSliderTrackerColor(int color) {
        mSliderTrackerColor = color;
        mHueAlphaTrackerPaint.setColor(new Color(mSliderTrackerColor));
        invalidate();
    }

    /**
     * Get color of the tracker slider on the hue and alpha panel.
     *
     * @return
     */
    public int getSliderTrackerColor() {
        return mSliderTrackerColor;
    }

    /**
     * Set the color of the border surrounding all panels.
     *
     * @param color
     */
    public void setBorderColor(int color) {
        mBorderColor = color;
        invalidate();
    }

    /**
     * Get the color of the border surrounding all panels.
     */
    public int getBorderColor() {
        return mBorderColor;
    }

    /**
     * Set the text that should be shown in the
     * alpha slider. Set to null to disable text.
     *
     * @param res string resource id.
     */
    public void setAlphaSliderText(int res) {
        String text = getContext().getString(res);
        setAlphaSliderText(text);
    }

    /**
     * Set the text that should be shown in the
     * alpha slider. Set to null to disable text.
     *
     * @param text Text that should be shown.
     */
    public void setAlphaSliderText(String text) {
        mAlphaSliderText = text;
        invalidate();
    }

    /**
     * Get the current value of the text
     * that will be shown in the alpha
     * slider.
     *
     * @return
     */
    public String getAlphaSliderText() {
        return mAlphaSliderText;
    }


    private class BitmapCache {
        public Canvas canvas;
        public PixelMap bitmap;
        public float value;
    }

}
