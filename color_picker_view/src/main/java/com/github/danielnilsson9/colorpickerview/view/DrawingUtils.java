package com.github.danielnilsson9.colorpickerview.view;


import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.app.Context;

public class DrawingUtils {

    public static int vpToPx(Context c, float dipValue) {
        return AttrHelper.vp2px(dipValue, c);
    }

}
